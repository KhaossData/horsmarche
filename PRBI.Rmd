---
title: "PRBI"
author: "Cité des Bâtisseurs"
output: 
  flexdashboard::flex_dashboard:
    social: menu
    source_code: embed
runtime: shiny
---

```{r global, include=FALSE}
# load data in 'global' chunk so it can be shared by all users of the dashboard
library (plyr)
library(stringr)
library(tidyr)
library(magrittr)
library(dplyr)
library(ggplot2)
donnees=read.csv("DATA/QC_Revenu_2016.csv",
             header = T,
             fileEncoding = "UTF8",
             check.names = F )%>%
  filter(ZIM!="RMR")%>%
  mutate(
MRC=str_replace(MRC, "\\s*\\([^\\)]+\\)", ""),
MRC=(str_sub(MRC,5)),
MenageAvecRevenu=TotalMenageAvecRevenu)
#donnees$RA=donnees$RA[!is.na(donnees$RA)]
#donnees$RA=donnees$RA[donnees$RA != ""]

```

## Inputs {.sidebar}

**NOTES**

En lien avec le PRBI, Cité des bâtisseurs a extrait les données de statistiques Canada 2016 sur les revenus des ménages. Vous pouvez afficher les données en fonction de deux variables :

-   Les Population;

-   La région administrative

Cette application n'intègre pas les données des entités suivantes :

-   Les municipalités situées dans les limites d'une région métropolitaine;

-   Les municipalités de plus de 25 000 habitants;

-   Les territoires non organisés (TNO);

-   Les territoire où la Loi sur l'aménagement et l'urbanisme n'est pas appliquée.

------------------------------------------------------------------------

```{r}
sliderInput(inputId='pop',
            label='Population',
            min=100,
            max=25000,
            value=c(250,2500),
            step=100,
            round=0)

selectInput(inputId='ra',
            label='Région',
            choices =as.factor(donnees$RA[!is.na(donnees$RA)]),
            selected = "Côte-Nord (09)"
)
```

## Column

```{r}
renderPlot({
revenuRA=filter(donnees,RA==input$ra & Population<input$pop,Admin %in% c("MÉ","V","VL","PE"))%>%
  mutate(Moins20k=(MoinsDe5k+r10k_14.9k+r15k_19.9k)/TotalMenageAvecRevenu*100)%>%
  dplyr::select(Nom,Moins20k)%>%
  arrange(Moins20k)
ggplot(revenuRA,aes(x=reorder(Nom,Moins20k), y=Moins20k))+
  geom_col()+coord_flip()+
  labs(title=input$ra,
       x="Municipalité",
       y="Ménages ayant des revenus inférieurs à 20k $ (%)",
       caption = "\nSource: StatCan 2016")
},width = 800, height=900)
```

## Column

```{r}
renderDataTable({
  filter(donnees,RA==input$ra & Population<input$pop)%>%
    dplyr::select(Nom,
                  Admin,
                  MRC,
                  Population,
                  MenageAvecRevenu,
                  MoinsDe5k,
                  r10k_14.9k,
                  r15k_19.9k)
}
)
```
